pragma solidity >=0.4.22 <0.6.0;
contract Bank {

    /*   
    Variabler 
    */
     
    // 1 
    bool isActive;
    // 4
    string name;
    // 7
    mapping(address => uint) balance;
    
    // 3
    constructor() public{
        isActive = true;
        // 5
        name = "Blockchangers ledger";
        // 8
        balance[msg.sender] = 1;
    }


    /*   
    Funksjoner 
    */

    // 2
    function isContractActive() public view returns(bool){
        return isActive;
    }
    
    // 6
    function getName() public view returns(string memory){
        return name;
    }
    
    // 9
    function getBalanceOfSender() public view returns(uint) {
        return balance[msg.sender];
    }
    
    // 10
    function setBalanceOfAddress(address adressToChangeBalance, uint value ) public //12  payable 
    // 16 notStopped
    {
        balance[adressToChangeBalance] = value;
        // 11
        //balance[adressToChangeBalance] += value;
        // 13
        //balance[adressToChangeBalance] += msg.value;
    }
    
    // 14
    function stop() public{
        isActive = false;
    }
    
    /*
    Modifiers
    */

    // 15
    modifier notStopped(){
        require(isActive, "Contract is not active");
        _;
    }
}
